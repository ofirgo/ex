<?php
class Htmlpage{
   protected $title="Home";
   protected $body="Hello World!";
   function __construct($title="",$body=""){
       if ($title!=""){
           $this->title=$title;
       }
        if ($body!=""){
           $this->body=$body;
       }
   }

   public function view(){
   echo "
   <html>
        <head>
            <title> 
               $this->title
            </title>
        </head>
        <body>
            $this->body
        </body>
    </html>";

   }
}

class Newhtml extends Htmlpage{
    protected $clr = 'green';
    protected $clrs=array('red','blue','green','black');
    public function __set($property,$value){
        if ($property=='clr'){   
                if(in_array($value,$this->clrs)){
                    $this->clr=$value;

            }
                else {
                    die($this->body="Invalid Color!");
                    
                   

                }
            
        }
    }

    public function view(){
        echo"<p style = 'color:$this->clr'>$this->body</p>";
    }
}

class Fontsize extends Newhtml{
    protected  $fnt=12;
    public function __set($property,$value){
        parent::__set($property,$value);
        $fnts= range(10,24);
        if ($property =='fnt'){   
                if (in_array($value,$fnts)){
                    $this->fnt=$value;
                }

                else{
                    die( "Invalid Font Size!");

                }        
        }


    }

    public function show(){
        echo"<p style ='color:$this->clr; font-size:$this->fnt'>$this->body</p>";
    }
}
?>
